local S = minetest.get_translator("sfinv")

gui_privs = {}
gui_privs.selection_index = {}
gui_privs.privs_index = {}

gui_privs.enable = function(playername, privname) 
	minetest.chat_send_player(playername, "you enable the privilege ".. privname)
	local privs = minetest.get_player_privs(playername)
	privs[privname] = true
	minetest.set_player_privs(playername, privs)
end

gui_privs.disable = function(playername, privname) 
	minetest.chat_send_player(playername, "you disabled the privilege ".. privname)
	local privs = minetest.get_player_privs(playername)
	privs[privname] = nil
	minetest.set_player_privs(playername, privs)
end

sfinv.register_page("gui_privs:privileges", {
	title = S("Privileges"),
	get = function(self, player, context)
		local privs = minetest.registered_privileges
		local priv_string = ""
		local first = true
		local i = 1
		for k, v in pairs(privs) do
			gui_privs.privs_index[i] = k
			i = i + 1
			local enabled = " - disabled"
			if minetest.get_player_privs(player:get_player_name())[k] then
				enabled = " - enabled"
			end
			if first then 
				first = false
			else
				k = "," .. k
			end
			k = k .. enabled
			priv_string = priv_string .. k
		end
		return sfinv.make_formspec(player, context, [[
				textlist[0,0;8,3.5;privileges;]]..priv_string..[[]
				button[0,4;2,1;enable;Enable]
				button[3,4;2,1;disable;Disable]
			]], true)
	end,
	is_in_nav = function(self, player, context)
		if minetest.get_player_privs(player:get_player_name())["privs"] then
			return true
		else
			return false
		end
	end,
	on_player_receive_fields = function(self, player, context, fields) 
		local playername = player:get_player_name()
		if fields.privileges ~= nil then
			local index = tonumber(fields.privileges:sub(5))
			gui_privs.selection_index[playername] = index
		end

		local selected_priv_idx = gui_privs.selection_index[playername]
		local selected_priv = gui_privs.privs_index[selected_priv_idx]
		if fields.enable ~= nil then
			gui_privs.enable(playername, selected_priv)
			sfinv.set_player_inventory_formspec(player, context)
		elseif fields.disable ~= nil then
			gui_privs.disable(playername, selected_priv)
			sfinv.set_player_inventory_formspec(player, context)
		end
	end,
})


